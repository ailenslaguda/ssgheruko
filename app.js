//[SECTION] Packages and Dependencies
  const express = require("express"); 
  const mongoose = require("mongoose");  
  const cors = require("cors");
  const dotenv = require("dotenv");  
  const candidateRoutes = require('./routes/candidates'); 
  const studentRoutes = require('./routes/students');
  const csvtojson = require("csvtojson");

//[SECTION] Server Setup
  const app = express(); 
  dotenv.config(); 
  app.use(express.json());
  app.use(cors());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT; 


//[SECTION] Application Routes
  app.use('/candidates', candidateRoutes);
  app.use('/students', studentRoutes);

//[SECTION] Database Connection
  mongoose.connect(secret)
  let connectStatus = mongoose.connection; 
  connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
  app.get('/', (req, res) => {
     res.send(`Welcome to Gallanosa National High School SSG Voting System`); 
  }); 

  app.listen(port, () => console.log(`Server is running on port ${port}`)); 


  function parseBool(value) {
    if (value === "TRUE") {
      return true
    } else {
      return false
    }
  }

//Upload using excel file

  // app.post("/upload-csv", uploads.single("csv"), (req, res) => {
    // const fileName = "sample.csv";
    // let arrayToInsert = [];

    // csvtojson().fromFile(fileName).then(source => {

    //     // Fetching the all data from each row

    //     for (let i = 0; i < source.length; i++) {
    //          let oneRow = {
    //              _id: parseInt(source[i]["LRN"]),
    //              firstName: source[i]["FirstName"],
    //              middleName: source[i]["MiddleName"],
    //              lastName: source[i]["LastName"],
    //              section: source[i]["Section"],
    //              sex: source[i]["Sex"],
    //              grade: source[i]["Grade"],
    //              password: source[i]["Password"],
    //              isAdmin: parseBool(source[i]["isAdmin"]),
    //              isActive: parseBool(source[i]["isActive"]),
    //             votingStatP: parseBool(source[i]["votingStatP"]),
    //             votingStatVP: parseBool(source[i]["votingStatVP"]),
    //             votingStatS: parseBool(source[i]["votingStatS"]),
    //             votingStatT: parseBool(source[i]["votingStatT"]),
    //             votingStatA: parseBool(source[i]["votingStatA"]),
    //             votingStatPIO: parseBool(source[i]["votingStatPIO"]),
    //             votingStatPO:parseBool(source[i]["votingStatPO"]),
    //             votingStatGLR: parseBool(source[i]["votingStatGLR"])

    //          };
    //          arrayToInsert.push(oneRow);
    //      }
    //      //inserting into the table "employees"
    //      let collectionName = 'students';
    //      let collection = connectStatus.collection(collectionName);
    //      collection.insertMany(arrayToInsert, (err, result) => {
    //          if (err) console.log(err);
    //          if(result){
    //              console.log("Import CSV into database successfully.");
    //          }
    //      });
    // });