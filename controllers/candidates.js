// [SECTION]: DEPENDENCIES AND MODULES
	const auth = require("../auth")
	const Student = require('../models/student');
	const Candidate = require('../models/candidate');
	const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 

// [SECTION]: ENVIRONMENT SET-UP 
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION]: FUNCTIONALITY - CREATE
	// create user individuall/register students
	module.exports.registerCandidate = (req, res) => {
			// This is the object variable for student to be added.
		let newCandidate = new Candidate({	
			_id: req.body.LRN,
			firstName: req.body.firstName,
			middleName: req.body.middleName,
			lastName: req.body.lastName,
			section: req.body.section,
			sex: req.body.sex,
			grade: req.body.grade,
			position: req.body.position,
			picPath: req.body.picPath,
			party: req.body.party
		});


		// saving the new entry to the database and returning the promise.
		return newCandidate.save().then( (studentCreated, rejected) => {
			
			if (studentCreated) {
				res.send (true);
			} else {
				res.send (false);
			}	
		})
		.catch(err => res.send(err))
	}

// [SECTION]: FUNCTIONALITY - RETRIEVE
	// get candidates name by position
		module.exports.getCandidateByPosition = (req, res) => {
				let position = req.params.position;
			
			return Student.find({_id:req.user._id}).then(result => {
				
				if(result === null){
					return res.send({message:"NULL"})
				
				} else if(result.isAdmin===true){
					return res.send(false)
				} else{
					return Candidate.find({position: position}).then(data => {

						if(result === null){
							return res.send(false)
						} else{
							return res.send(data)
						}
					})
					.catch(err => res.send(err))
				}

			})
			.catch(err => res.send(err))
			
		}

		module.exports.getCandidateByPositionAdmin = (req, res) => {
				
			let position = req.params.position;

			return Candidate.find({position: position}).then(data => {

					if(data === null){
						return res.send(false)
					} else{
						return res.send(data)
					}
			})
			.catch(err => res.send(err))
		}

		module.exports.getCandidateByLRN = (req, res) => {
				
			let LRN = req.params.LRN;

			return Candidate.findOne({_id: LRN}).then(data => {

					if(data === null){
						return res.send(false)
					} else{
						return res.send(data)
					}
			})
			.catch(err => res.send(err))
		}
	// get candidates name by position and grade level
		module.exports.getCandidateByPositionAndGradeLevel = (req, res) => {
				let position = req.params.position;
				let grade =req.params.grade
			
			return Student.find({_id:req.user._id}).then(result => {
				
				if(result === null){
					return res.send({message:"NULL"})
				
				} else if(result.isAdmin===true){
					console.log(result)
					return res.send(false)
				} else{
					return Candidate.find({position: position, grade:req.params.grade}).then(data => {

						if(result === null){
							return res.send(false)
						} else{
							return res.send(data)
						}
					})
					.catch(err => res.send(err))
				}

			})
			.catch(err => res.send(err))
			
		}

// [SECTION]: FUNCTIONALITY - UPDATE
	// Update the candidate info 
	module.exports.updateCandidate = (req, res) => {

		let updatedCandidate = {	
			firstName: req.body.firstName,
			middleName: req.body.middleName,
			lastName: req.body.lastName,
			section: req.body.section,
			sex: req.body.sex,
			grade: req.body.grade,
			position: req.body.position,
			picPath: req.body.picPath,
			party: req.body.party
			
		};


		// updating the db.
		return Candidate.findByIdAndUpdate(req.params.LRN, updatedCandidate).then((candidateUpdated, err) => {
	        
	        if (err) {
	          res.send (false)
	        } else {
	          res.send(true);
	        }

	    })
	    .catch(err => res.send(err))
	}

	// Update voteCount 
	module.exports.updateCandidateVote = (req, res) => {

		let presentVoteCount = 0;
		let id = req.body.LRN;

		return Candidate.findById(id).then(result => {
	        
	      	presentVoteCount = result.voteCount+1;

	      	let updatedCandidate = {	
			
				voteCount: presentVoteCount
			};


			// updating the db.
			return Candidate.findByIdAndUpdate(req.body.LRN, updatedCandidate).then((candidateUpdated, err) => {
		        
		        if (err) {
		          res.send (false)
		        } else {
		          res.send(true);
		        }

		    })
		    .catch(err => res.send(err))
	    })
	    .catch(err => res.send(err))
		
	}	 
	// Reset vote counts 
	module.exports.resetVote= (req, res) => {

		return Candidate.updateMany({}, {$set: {voteCount: 0}}).then((voteResetSuccess, err) => {
	        
	        if (err) {
	          res.send ({message:`Failed to update. Error: ${err}`})
	        } else {
	          res.send({message:`All vote counts has been reset back to zero!`});
	        }

	    })
	    .catch(err => res.send(err))

	}

// [SECTION]: FUNCTIONALITY - DELETE
	// delete candiate
	 	module.exports.delete_candidate = (req, res) =>{

	 		let id = req.params	.LRN;

		    return Candidate.findByIdAndRemove(id).then((removedUser, err)=>{
		     
		      if (removedUser) {
		        return res.send({message:` A candidate has been removed as candidate`});
		      } else {
		        return res.send ({message:`Failed to update. Error: ${err}`})
		      }

		    })
		    .catch(err => res.send(err))
	   }
