// [SECTION]: DEPENDENCIES AND MODULES
	const auth = require("../auth")
	const Student = require('../models/student');
	const Candidate = require('../models/candidate');
	// const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 	
	  const cors = require("cors");


// [SECTION]: ENVIRONMENT SET-UP 
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION]: FUNCTIONALITY - CREATE
	// create user individuall/register students
	
	module.exports.registerStudent = (req, res) => {
		
		// This is the object variable for student to be added.
		let newStudent = new Student({	
			_id: req.body.LRN,
			firstName: req.body.firstName,
			middleName: req.body.middleName,
			lastName: req.body.lastName,
			section: req.body.section,
			sex: req.body.sex,
			grade: req.body.grade,
			password: req.body.password
			// password: bcrypt.hashSync(req.body.password,salt)
		});


		// saving the new entry to the database and returning the promise.
		return newStudent.save().then( (studentCreated, rejected) => {
			
			if (studentCreated) {
				res.send (true);
			} else {
				res.send (false);
			}	
		})
		.catch(err => res.send(err))
	};

	
// [SECTION]: FUNCTIONALITY - RETRIEVE
	 //Login
	module.exports.login = (req, res) => {
		
	

	 	let LRN = req.body._id;
	 	let password = req.body.password;
	 
	 	console.log(LRN)
 		return Student.findOne({_id:LRN}).then(result => {
 		
			if (result === null){
			
				res.send ({message:('Student not found')});

			} else if (result.isActive === false){
			
				res.send ({message:('Student already voted')});
			
			}else {

				if(password === result.password){

			          res.send ( {accessToken: auth.createAccessToken(result), firstName: result.firstName, lastName: result.lastName, middleName:result.middleName, isAdmin: result.isAdmin, _id:result._id, grade:result.grade, isActive:result.isActive});
			      
			       	} else {
			           res.send ({message: ("Incorrect Password")});
			    }
			
				// const isPasswordCorrect = bcrypt.compareSync(password, result.password)
		     
		      
			}

		})
		.catch(err => res.send(err))
	}

	// retrieve students name for candidate filing
	module.exports.getStudentsData = (req, res) => {

		return Student.find({}).then(result => {

			if(result === null){
				res.send(null)
			} else{
				return res.send(result)
			}

		})
		.catch(err => res.send(err))
	}

	module.exports.getStudentData = (req, res) => {

			return Student.findById(req.params.LRN).then(result => {
				// console.log(result)
				if(result === null){
					res.send(null)
				} else{
					return res.send(result)
				}

			})
			.catch(err => res.send(err))
	}

	module.exports.getStudentVotingStatus = (req, res) => {

		let totalVoter=0;
		let totalWhoVoted = 0;
		let totalWhoDidNotLogOut = 0;

		return Student.find({}).then(totalVoters => {

			totalVoter = totalVoters.length;

			return Student.find({isActive:false}).then(count => {
				
				totalWhoVoted=count.length;
				
				return Student.find({votingStatP:true, isActive:true}).then(count => {
					console.log(count)
					totalWhoDidNotLogOut=count.length;
					return res.send({totalVoter,totalWhoVoted, totalWhoDidNotLogOut})
				
				}).catch(err => res.send(err))
			
			}).catch(err => res.send(err))

		})
		.catch(err => res.send(err))
	}


// [SECTION]: FUNCTIONALITY - UPDATE
	module.exports.updateStudent = (req, res) => {
		
		// This is the object variable for student to be added.
		let updatedStudent ={	
		
			firstName: req.body.firstName,
			middleName: req.body.midleName,
			lastName: req.body.lastName,
			section: req.body.section,
			sex: req.body.sex,
			grade: req.body.grade,
			password: req.body.password
		};


		// updating the db.
		return Student.findByIdAndUpdate(req.params.LRN, updatedStudent).then((studentUpdated, err) => {
	        
	        if (err) {
	          res.send (false)
	        } else {
	          res.send(true);
	        }

	    })
	    .catch(err => res.send(err))
	};

	module.exports.updateStudentVoteStat = (req, res) => {
		
		// This is the object variable for student to be added.
		let position = req.body.position;
		console.log(position)
		console.log(req.user._id)
		let updatedStudent ={}

			switch (position){
				case "President":
					updatedStudent = {votingStatP:true}
					break;
				case  "Vice President":
					updatedStudent = {votingStatVP:true}
					break;
				case  "Secretary":
					updatedStudent = {votingStatS:true}
					break;
				case  "Treasurer":
					updatedStudent = {votingStatT:true}
					break;
				case  "Auditor":
					updatedStudent = {votingStatA:true}
					break;
				case  "PIO":
					updatedStudent = {votingStatPIO:true}
					break;
				case  "Protocol Officer":
					updatedStudent = {votingStatPO:true}
					break;
				case  "Grade Level Representative":
					updatedStudent = {votingStatGLR:true}
					break;
				}


		// updating the db.
		return Student.findByIdAndUpdate(req.body.LRN, updatedStudent).then((studentUpdated, err) => {
	        
	        if (err) {
	          res.send (err)
	        } else {
	          res.send(true);
	        }

	    })
	    .catch(err => res.send(err))
	};

	module.exports.updateStudentActiveStat = (req, res) => {

		return Student.findByIdAndUpdate(req.user.id, {isActive: false}).then((studentUpdated, err) => {
	        
	        if (err) {
	          res.send (err)
	        } else {
	          res.send(true);
	          console.log(req.user.id)
	        }

	    })
	    .catch(err => res.send(err))
	
	};


// [SECTION]: FUNCTIONALITY - DELETE
	// delete candiate
	 	module.exports.delete_student = (req, res) =>{

	 		let id = req.params.LRN;

		    return Student.findByIdAndRemove(id).then((removedUser, err)=>{
		     
		      if (removedUser) {
		        return res.send({message:` A student has been removed.`});
		      } else {
		        return res.send ({message:`Failed to update. Error: ${err}`})
		      }

		    })
		    .catch(err => res.send(err))
	   }
	// delete all students
	 	module.exports.deleteAllStudent = (req, res) => {
		  return Student.deleteMany({ '_id': { $ne: '111' } })
		    .then(removedUsers => {
		      return res.send({ message: `${removedUsers.deletedCount} students removed.` });
		    })
		    .catch(err => {
		      return res.send({ message: `Failed to update. Error: ${err}` });
		    });
		};
