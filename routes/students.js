// [SECTION]: Dependencies and Modules
  const exp = require("express");
  const User = require('../models/student');
  const controller = require('../controllers/students.js');
  const auth = require("../auth");



  // destructure verify from auth
    const {verify, verifyAdmin} = auth;

// [SECTION]: Routing Component
    const route = exp.Router(); 
  
// [SECTION]: POST ROUTES (Create)
  // register new student
  route.post("/register", controller.registerStudent);
  // route.post('/upload-csv', ;

  // upload excel file

// [SECTION]: GET ROUTES (Retrieve)
  // Login
  route.post("/login", controller.login);

  // retrieve students name
  route.get('/getStudentsData', verify, verifyAdmin, controller.getStudentsData) 
  route.get('/:LRN/getStudentData', verify, controller.getStudentData) 
  route.get('/getStudentVotingStatus', verify, verifyAdmin, controller.getStudentVotingStatus) 


// [SECTION]: PUT ROUTES (Update)
  // update students data
  route.put('/:LRN/student_update', verify, verifyAdmin, controller.updateStudent)
  route.put('/student_update_vote', verify, controller.updateStudentVoteStat)
  route.put('/student_update_activeStat', verify, controller.updateStudentActiveStat)

// [SECTION]: FUNCTIONALITY - DELETE
  route.delete('/:LRN/delete_student', verify, verifyAdmin, controller.delete_student)
  route.delete('/deleteAllStudent', controller.deleteAllStudent)
  // route.delete('/deleteAllStudent', verify, verifyAdmin, controller.deleteAllStudent)

// [SECTION]: Export Route System
  module.exports = route;