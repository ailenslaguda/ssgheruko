// [SECTION]: Dependencies and Modules
  const exp = require("express");
  const User = require('../models/candidate');
  const controller = require('../controllers/candidates.js');
  const auth = require("../auth");

  // destructure verify from auth
    const {verify, verifyAdmin} = auth;

// [SECTION]: Routing Component
    const route = exp.Router(); 
  
// [SECTION]: POST ROUTES (Create)
  // register new student
  route.post("/register_candidate", verify, verifyAdmin, controller.registerCandidate);

// [SECTION]: POST ROUTES (Retrieve)
    // retrieve candidates by position
  route.get('/:position/get_candidate', verify, controller.getCandidateByPosition)
  route.get('/:position/get_candidate_admin', verify, verifyAdmin, controller.getCandidateByPositionAdmin)
  route.get('/:LRN/get_candidate_byID', verify, verifyAdmin, controller.getCandidateByLRN)
  route.get('/:position/:grade/get_candidate', verify, controller.getCandidateByPositionAndGradeLevel)

//[SECTION ]: PUT ROUTES (Update) 
  // route for updating candidate information
  route.put('/:LRN/update_candidate',  controller.updateCandidate)

  // route for updating votes
  route.put('/update_vote', verify, controller.updateCandidateVote)

  // route for updating votes
  route.put('/reset_vote', verify, verifyAdmin, controller.resetVote)


// [SECTION]: FUNCTIONALITY - DELETE
  route.delete('/:LRN/delete_candidate', verify, verifyAdmin, controller.delete_candidate)

// [SECTION]: Export Route System
  module.exports = route;

