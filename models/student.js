//[SECTION] Dependencies and Modules
  const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const studentSchema = new mongoose.Schema({
    
    _id:{
        type: Number,
        required: [true, 'LRN is required']
    },
    firstName:{
            type: String,
            required: [true, 'First Name is required']
    },
    middleName:{
        type: String	
    },
    lastName:{
        type: String,
        required: [true, 'Last Name is required']
    },
    section: {
        type: String,
        required: [true, 'Section is required'],

      },
    sex: {
        type: String,
        required: [true, 'Sex is required']
      },
    grade: {
        type: Number,
        required: [true, 'Grade is required']
    },
     password: {
        type: String,
        required: [true, 'Password is required']
      },
    isAdmin: {
        type: Boolean,  
        default: false 
    },
    isActive: {
        type: Boolean,
        default: true
    },
    votingStatP: {
        type: Boolean,
        default: false
    },
    votingStatVP: {
        type: Boolean,
        default: false
    },
    votingStatS: {
        type: Boolean,
        default: false
    },
    votingStatT: {
        type: Boolean,
        default: false
    },
    votingStatA: {
        type: Boolean,
        default: false
    },
    votingStatPIO: {
        type: Boolean,
        default: false
    },
    votingStatPO: {
        type: Boolean,
        default: false
    },
    votingStatGLR: {
        type: Boolean,
        default: false
    }
  })

//[SECTION] Model
   
   const Student = mongoose.model("Student", studentSchema); 
   module.exports = Student; 