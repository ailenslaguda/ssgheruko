//[SECTION] Dependencies and Modules
  const mongoose = require("mongoose"); 

//[SECTION] Schema / Document Blueprint
  const candidateSchema = new mongoose.Schema({
    
    _id:{
        type: Number,
        required: [true, 'LRN is required'] 
        // LRN ang magiging id 
    },
    firstName:{
            type: String,
            required: [true, 'First Name is required']
    },
    middleName:{
        type: String,
        required: [true, 'Last Name is required']
    },
    lastName:{
        type: String,
        required: [true, 'Last Name is required']
    },
    section: {
        type: String,
        required: [true, 'Section is required'],

    },
    grade: {
        type: Number,
        required: [true, 'Grade is required'],

    },
    sex: {
        type: String,
        required: [true, 'Sex is required']
      },
    position: {
        type: String,  
         required: [true, 'Position is required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    voteCount: {
        type: Number,
        default: 0
    },
    picPath: {
        type: String
    },
    party: {
        type: String
    }
  })

//[SECTION] Model
   
   const Candidate = mongoose.model("Candidate", candidateSchema); 
   module.exports = Candidate; 